<%@ include file="common/header.jspf"%>
<%@ include file="common/navigation.jspf"%>
<div class="container">

	<div class="panel panel-primary">
		<%-- <div class="panel-heading">Home Page</div> --%>
		<div class="panel-body">
		<div class="jumbotron">
			<h1 class="display-4">Welcome, ${name}!! </h1>
			<%-- <p class="lead">This is a simple hero unit, a simple jumbotron-style component for calling extra attention to featured content or information.</p>
			<hr class="my-4">
			<p>It uses utility classes for typography and spacing to space content out within the larger container.</p> --%>
			<a class="btn btn-primary btn-md" href="/list-todos" role="button">Click here</a><h4>to manage your
			todo's.</h4>
		</div>
			<a href="/list-todos"></a>
		</div>
	</div>
</div>
<%@ include file="common/footer.jspf"%>