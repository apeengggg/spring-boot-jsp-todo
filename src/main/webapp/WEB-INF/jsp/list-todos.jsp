<%@ include file="common/header.jspf"%>
<%@ include file="common/navigation.jspf"%>

<div class="container mt-3">
	<div>
		<a type="button" class="btn btn-primary btn-md" href="/add-todo">(+) Add Todo</a>
	</div>
	<br>
	<div class="panel panel-primary">
		<div class="panel-heading mb-2">
			<h3>List of ToDo's</h3>
		<%-- <a type="button" class="btn btn-info btn-md" href="/add-todo">Complete</a>
		<a type="button" class="btn btn-danger btn-md" href="/add-todo">Uncomplete</a> --%>
		</div>
		<div class="panel-body">
			<table class="table table-bordered">
				<thead class="thead-dark" align="center">
					<tr>
						<%-- <th width="5%"></th> --%>
						<th width="45%">Description</th>
						<th width="20%">Target Date</th>
						<%-- <th width="10%">Status</th> --%>
						<th width="20%">Action</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${todos}" var="todo">
						<tr>
							<%-- <td align="center"><input type="checkbox" value="${todo.id}"></td> --%>
							<td>${todo.description}</td>
							<td align="center"><fmt:formatDate value="${todo.targetDate}"
									pattern="dd/MM/yyyy" /></td>
							<%-- <td align="center">${todo.status}</td> --%>
							<td align="center"><a type="button" class="btn btn-success"
								href="/update-todo?id=${todo.id}">Update</a>
							<a type="button" class="btn btn-warning" onclick="return confirm('Are you sure you want to delete this ToDo item?');"
								href="/delete-todo?id=${todo.id}">Delete</a></td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</div>

</div>
<%@ include file="common/footer.jspf"%>